<?php
include_once "../../../../Src/Bitm/SEIP/Students/Students.php";
use App\Bitm\SEIP\Studens\Students;
$obj = new Students();
$value=$obj->setData($_GET)->show();
?>
<html>
<head>
    <title>List of Student </title>
    <style>
        table {
            border-collapse: collapse;
            width: 80%;

        }

        th, td {
            text-align: center;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
<a href="index.php">Back to list</a>
<table>
    <tr>
        <th>Serial</th>
        <th>Title </th>
        <th>Action</th>
    </tr>

        <tr>
            <td><?php echo $value ['id'] ?></td>
            <td><?php echo $value['title']; ?></td>
            <td>
                <a href=edit.php> Edit </a>
            </td>
        </tr>
</table>
</body>
</html>
