<?php
include_once "../../../../Src/Bitm/SEIP/Students/Students.php";
use App\Bitm\SEIP\Studens\Students;
$obj = new Students();
$alldata = $obj->index();
session_start();

if(isset( $_SESSION['message']))
{
    echo $_SESSION['message'];
    unset( $_SESSION['message']);
}
?>

<html>
<head>
    <title>List of Student </title>
    <style>
        table {
            border-collapse: collapse;
            width: 80%;

        }

        th, td {
            text-align: center;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
<a href="create.php">Add new</a>
    <table>
        <tr>
            <th>Serial</th>
            <th>Title </th>
            <th>Action</th>
        </tr>
        <?php
        $serial =1;
        foreach ($alldata as $key =>$value){ ?>

        <tr>
            <td><?php echo $serial++ ?></td>
            <td><?php echo $value['title']; ?></td>
            <td>
                <a href="show.php?id=<?php echo $value ['id']   ?>"> View </a>
                <a href="Delete.php?id=<?php echo $value ['id']   ?>"> Delete </a>
            </td>

        </tr>
        <?php  } ?>
    </table>
</body>
</html>