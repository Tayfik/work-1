<?php
namespace App\Bitm\SEIP\Studens;
use PDO;
class Students
{
    public $id = '';
    public $name = '';
    public $pass = '';
    public $mail ='';

    public function setData($data = '')
    {
        if (array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if (array_key_exists('title',$data)){
            $this->name = $data ['title'];
        }
        if (array_key_exists('pass',$data)){
            $this->pass = $data ['pass'];
        }
        if (array_key_exists('email',$data)){
            $this->mail = $data ['email'];
        }

        return $this;
    }

    public function index(){

        try {
            $pdo = new PDO('mysql:host=localhost;dbname=project1', 'root', '');
            $query = "SELECT * FROM `students`";

            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data=$stmt->fetchAll();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=project1', 'root', '');
            $query = "INSERT INTO `students` (`id`, `title`, `pass`,`email`) VALUES (:a,:b,:c,:d);";

            $stmt = $pdo->prepare($query);
            $stmt->execute(
                array(
                ':a' => null,
                ':b' => $this->name,
                ':c' =>$this->pass,
                'd' =>$this->mail
            ));
            if ($stmt){
                session_start();
                $_SESSION['message'] = "succesfully submitted";
                header('location:Student.php');
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

    public  function show()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=project1', 'root', '');
            $query = "SELECT * FROM `students` WHERE id=$this->id";

            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data=$stmt->fetch();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function delete()
    {

        try {
            $pdo = new PDO('mysql:host=localhost;dbname=project1', 'root', '');
            $query = "DELETE FROM `students` WHERE `students`.`id` = $this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if ($stmt){
                session_start();
                $_SESSION ['message']="Delete Succesfully";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update()
    {    // echo "<pre>";
           //var_dump($this);
        //die("update here");

        try {
            $pdo = new PDO('mysql:host=localhost;dbname=project1', 'root', '');
            $query = "UPDATE `students` SET `title` = :name, `pass` = :pass, `email` = :mail WHERE `students`.`id` = :id";

            $stmt = $pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':name' => $this->name,
                    ':pass' =>$this->pass,
                    ':mail' =>$this->mail
                ));
            if ($stmt){
                session_start();
                $_SESSION['message'] = "update Succesfully";
                header('location:index.php');
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }
}

