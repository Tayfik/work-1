<?php
include_once "../../../../vendor/autoload.php";
use App\Bitm\SEIP\Students\Student;
$object = new Student();
$data = $object->index();
?>

<html>
<head>
    <title></title>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: center;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
<table>
    <tr>
        <th>ID</th>
        <th>User Name</th>
        <th>Action</th>
    </tr>
    <?php
    foreach ($data as $key =>$value){
        ?>
        <tr>
            <td><?php echo $value['id'];?></td>
            <td><?php echo  $value['Fname']; ?></td>
            <td>View |  Edit | Delete</td>
        </tr>

    <?php } ?>
</table>
</body>
</html>