<?php
include_once "../../../../vendor/autoload.php";
use App\Bitm\SEIP\Students\Student;
$object = new Student();
$value = $obj->setData($_GET)->show();
?>

<html>
<head>
    <title></title>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: center;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
<table>
    <tr>
        <th>ID</th>
        <th>User Name</th>
        <th>Action</th>
    </tr>

        <tr>
            <td><?php echo $value['id'];?></td>
            <td><?php echo  $value['Fname']; ?></td>
            <td>
                <a href="show.php?id=<?php echo $value['id'] ?>"> Edit | Delete</a>
               </td>
        </tr>


</table>
</body>
</html>
