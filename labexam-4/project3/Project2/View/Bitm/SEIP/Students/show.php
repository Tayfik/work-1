<?php
//include_once "../../../../Src/Bitm/SEIP/Students/Students.php";
include_once "../../../../vendor/autoload.php";
use App\Bitm\SEIP\Students\Students;
$obj = new Students();
$value=$obj->setData($_GET)->show();
?>
<html>
<head>
    <title>List of Student </title>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            background-color: ;
            text-align: left;
            padding: 8px;
        }

        th {
            background-color: darkseagreen;
        }
    </style>
</head>
<body>
<a href="index.php">Back to list</a>
<table>
    <tr>
        <th>ID</th>
        <th>Title </th>
        <th>Email</th>
        <th>Action</th>

    </tr>
    <tr>
        <td><?php echo $value['id']; ?></td>
        <td><?php echo $value['title']; ?></td>
        <td><?php echo $value['email']; ?></td>
        <td>
            <a href="edit.php?id=<?php echo $value ['id'] ?>"> Edit </a>
        </td>
    </tr>
</table>
</body>
</html>
