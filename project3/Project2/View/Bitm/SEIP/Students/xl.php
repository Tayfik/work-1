<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_error',TRUE);
ini_set('display_startup_errors',TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die("This example should only be run froma Web Browser");
/** include PHPExcel */

require_once "../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php";
require_once "../../../../vendor/autoload.php";
use App\Bitm\SEIP\Students\Students;
$obj = new Students();
$alldata = $obj->index();

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
//Set document propertice

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Text Document for office 2007 XLSX, generated using PHP classes ")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Text result file");

    //Add some date
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1' , 'SL')
    ->setCellValue('B1' , 'ID')
    ->setCellValue('C1', 'Title');
$counter = 2;
$serial = 0;
foreach ($alldata as $data) {
    $serial++;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' .$counter, $serial)
        ->setCellValue('B' .$counter, $data['id'])
        ->setCellValue('C' .$counter, $data['title']);
        $counter++;
        }
$objPHPExcel->getActiveSheet()->setTitle('Mobile_List');


//set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client's web browser (Excel)
header('Content-Type: application/vnd.ms-excel ');
header('Content_Disposition: attatchment;filename="01simple.xls');
header('Cache-Control: max-age=0');
//If you're serving to IE 9, then the following maybe needed
header('Cache-Control:max-age=1');

//If you're serving to IE over SSL, then the following maybe needed
header('Expires: Mon,26 Jul 1997 05:00:00 GMT'); //Date in the past
header('Last-Modified: '.gmdate('D, d M Y H:i:s').'GMT'); //always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); //HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit();



