<?php
//include_once "../../../../Src/Bitm/SEIP/Students/Students.php";
include_once "../../../../vendor/autoload.php";
use App\Bitm\SEIP\Students\Students;
$obj = new Students();

$alldata = $obj->index();
session_start();

if(isset( $_SESSION['message']))
{
    echo $_SESSION['message'];
    unset( $_SESSION['message']);
}
?>

<html>
<head>
    <title>List of Student </title>
    <link type="text/css" rel="stylesheet" href="../../../../asset/css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="../../../../asset/css/font-awesome.css">

    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            background-color: ;
            text-align: left;
            padding: 8px;
        }

        th {
            background-color: darkseagreen;
        }
    </style>
    <script src="http://ajax.googleleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

</head>
<body>
<a href="create.php">Add new</a>
<a href="create.php">Create New</a>
<br>

<span id="utility">Download as <a href="pdf.php">PDF</a> <i class="fa fa-download" aria-hidden="true" style="cursor: pointer"></i> </span></br>
<span id="utility">Download as <a href="xl.php">XL</a> <i class="fa fa-download" aria-hidden="true" style="cursor: pointer"></i> </span>

<form class="form-inline quick-search-form" role="form" action="search.php" method="get">
    <div class="form-group">
        <input type="text" class="form-control" placeholder="Search here">
    </div>
    <button type="submit" id="quick-search" class="btn btn-custom">
        <span class="glyphicon glyphicon-search custom-glyph-color"></span></button>
</form>

    <table>
        <tr>
            <th>Serial</th>
            <th>User name </th>
            <th>password</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
        <?php
        $serial =1;
        foreach ($alldata as $key =>$value){ ?>

        <tr>
            <td><?php echo $serial++ ?></td>
            <td><?php echo $value['title' ]; ?></td>
            <td><?php echo $value['pass']; ?></td>
            <td><?php echo $value['email']; ?></td>
            <td>
                <a href="show.php?id=<?php echo $value ['id']   ?>"> View </a>
                <a href="Delete.php?id=<?php echo $value ['id']   ?>"> Delete </a>
            </td>

        </tr>
        <?php  } ?>
    </table>
</body>
</html>