<?php


$pdo = new PDO('mysql:host=localhost;dbname=datawork', 'root', '');
$query = "SELECT * FROM `data_entry`";

$stmt = $pdo->prepare($query);
$stmt->execute();
$datas=$stmt->fetchAll();


if(isset($_POST['selection'])){
    $title = $_POST['selection'];
    $sql ="INSERT INTO `entry_data` (`id`, `token`, `option`) VALUES (NULL,'$title');";
    $res=$pdo->query($sql);

    var_dump($res);

    die();
//    if ($res === TRUE) {
//        echo "successfully";
//    } else {
//        echo "faield: ";
//    }
}


?>


<html>
    <head>
        <title>multiple select</title>
        <script src="JS/jquery.js" type="text/javascript"></script>
        <script >
            $(document).ready(function () {

                newrow = function (e) {
                    var row = $(e).parents('tbody');
                    $(e).parents('tr').after("<tr>" + row.children("tr").first().html() + "</tr>");
                }
                removeLast = function () {
                    if ($("#tab").children('tbody').children('tr').length > 1)
                        $("#tab").children('tbody').children('tr').last().remove();
                }
                selectChanged = function (e) {
                    var val = $(e).val();
                    $(e).parent('td').prev('td').children("input").val(val);
                }
                inputChanged = function (e) {
                    var val = $(e).val();
                    $(e).parent('td').next('td').children("select").val(val);
                }
            });
        </script>
        <link type="text/css" rel="stylesheet" href="css/style.css">
    </head>
    
    <body>
    <form action="index.php" method="post">
        <table id="tab">
            <thead>
                <tr>

                    <th>ID</th>
                    <th>Caption</th>
                    <th>Add</th>
                </tr>
            </thead>

            <tbody>

                <tr>
                    <td><input class="enc"onkeyup="inputChanged(this)" id="id" type="text" name="id[]" value=""></td>
                    <td>
                        <select name="selection" id="title" onchange="selectChanged(this)">
                            <option> Select option </option>
                            <?php foreach($datas as $data ) { ?>
                            <option value="<?php echo $data['id']; ?>"><?php echo $data['cat_or_option'] ?></option>
                         <?php  }?>

                        </select>
                    </td>
                    <td>
                        <input id="bt" type="button" value="+" onclick="newrow(this)">
                    </td>
                </tr>
            </tbody>

            <button id="btn" onclick="removeLast()" >Remove</button>
            <button type="submit" id="btn" >Save</button>
        </table>
    </form>
    </body>
</html>
