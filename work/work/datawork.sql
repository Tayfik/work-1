-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2017 at 09:16 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `datawork`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_entry`
--

CREATE TABLE `data_entry` (
  `id` int(11) NOT NULL,
  `cat_or_option` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_entry`
--

INSERT INTO `data_entry` (`id`, `cat_or_option`) VALUES
(1, '100% schoolership uk'),
(2, '100% schoolership usa'),
(3, '100% schoolership bnagladesh'),
(4, '100% schoolership india');

-- --------------------------------------------------------

--
-- Table structure for table `entry_data`
--

CREATE TABLE `entry_data` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `option` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entry_data`
--

INSERT INTO `entry_data` (`id`, `token`, `option`) VALUES
(3, '1', '100% schoolership uk'),
(4, '588ef418c5bcb', '100% schoolership uk'),
(5, '588ef453d134f', '1'),
(6, '588ef4b35bf0c', '1'),
(7, '588ef4ba5a591', '100% schoolership bnagladesh'),
(8, '588ef4f49ac51', '100% schoolership usa'),
(9, '588ef576ed533', '1'),
(10, '', '1'),
(11, '', ''),
(12, '', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_entry`
--
ALTER TABLE `data_entry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entry_data`
--
ALTER TABLE `entry_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_entry`
--
ALTER TABLE `data_entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `entry_data`
--
ALTER TABLE `entry_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
